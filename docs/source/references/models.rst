.. _models:

======
Models
======

.. toctree::

   model_groups/photon_generation_models.rst
   model_groups/optical_models.rst
   model_groups/charge_generation_models.rst
   model_groups/charge_collection_models.rst
   model_groups/phasing_models.rst
   model_groups/charge_transfer_models.rst
   model_groups/charge_measurement_models.rst
   model_groups/readout_electronics.rst
