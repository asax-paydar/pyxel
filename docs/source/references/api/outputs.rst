.. _outputs_api:

=======
Outputs
=======

.. currentmodule:: pyxel.outputs

ExposureOutputs
---------------
.. autoclass:: ExposureOutputs
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

ObservationOutputs
------------------
.. autoclass:: ObservationOutputs
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

CalibrationOutputs
------------------
.. autoclass:: CalibrationOutputs
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:
