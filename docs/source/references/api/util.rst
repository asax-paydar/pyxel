.. _util_api:

=================
Utility functions
=================

.. currentmodule:: pyxel.util

Downloading examples
====================

download_examples
-----------------
.. autofunction:: download_examples

Memory usage
============

get_size
--------
.. autofunction:: get_size

memory_usage_details
--------------------

.. autofunction:: memory_usage_details

Timing
======

time_pipeline
-------------
.. autofunction:: time_pipeline

Image utilities
===============

fit_into_array
--------------
.. autofunction:: fit_into_array
