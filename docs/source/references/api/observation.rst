.. _observation_api:

===========
Observation
===========

.. currentmodule:: pyxel.observation

Observation
-----------

.. autoclass:: Observation
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

ParametricResult
----------------

.. autoclass:: ObservationResult
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

log_parameters
--------------
.. autofunction:: log_parameters