.. _pipelines_api:

=========
Pipelines
=========

.. currentmodule:: pyxel.pipelines

DetectionPipeline
-----------------
.. autoclass:: DetectionPipeline
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

Processor
---------
.. autoclass:: Processor
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

ModelGroup
----------
.. autoclass:: ModelGroup
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

ModelFunction
-------------
.. autoclass:: ModelFunction
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:


