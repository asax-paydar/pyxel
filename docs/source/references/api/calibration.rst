.. _calibration_api:

===========
Calibration
===========

.. currentmodule:: pyxel.calibration

Calibration
-----------
.. autoclass:: Calibration
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

CalibrationResult
-----------------
.. autoclass:: CalibrationResult
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

MyArchipelago
-------------
.. autoclass:: MyArchipelago
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

Algorithm
---------
.. autoclass:: Algorithm
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

Fitness functions
-----------------

sum_of_abs_residuals
====================
.. autofunction:: sum_of_abs_residuals

sum_of_squared_residuals
========================
.. autofunction:: sum_of_squared_residuals

reduced_chi_squared
===================
.. autofunction:: reduced_chi_squared
