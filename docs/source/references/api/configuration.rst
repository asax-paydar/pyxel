.. _configuration_api:

=============
Configuration
=============
.. currentmodule:: pyxel

Configuration
-------------
.. autoclass:: Configuration
    :members:
    :undoc-members:
    :exclude-members:

load
----
.. autofunction:: load

save
----
.. autofunction:: save

