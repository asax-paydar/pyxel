.. _notebook_api:

========
Notebook
========

.. currentmodule:: pyxel

display_detector
----------------
.. autofunction:: display_detector

display_html
------------
.. autofunction:: display_html

Displaying calibration inputs and outputs in notebooks
------------------------------------------------------

display_calibration_inputs
==========================
.. autofunction:: display_calibration_inputs

display_simulated
=================
.. autofunction:: display_simulated

display_evolution
=================
.. autofunction:: display_evolution

optimal_parameters
==================
.. autofunction:: optimal_parameters

champion_heatmap
================
.. autofunction:: champion_heatmap
