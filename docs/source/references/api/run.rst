.. _run_api:

===
Run
===
.. currentmodule:: pyxel

exposure_mode
-------------
.. autofunction:: exposure_mode

observation_mode
----------------
.. autofunction:: observation_mode

calibration_mode
----------------
.. autofunction:: calibration_mode

run
---
.. autofunction:: run