.. _detectorproperties_api:

===================
Detector properties
===================
.. currentmodule:: pyxel.detectors

Environment
-----------
.. autoclass:: Environment
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

Characteristics
---------------
.. autoclass:: Characteristics
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

Geometry
--------
.. autoclass:: Geometry
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

CCD specific
------------
:term:`CCD` specific classes.

CCDCharacteristics
==================
.. autoclass:: CCDCharacteristics
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

CCDGeometry
===========
.. autoclass:: CCDGeometry
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

CMOS specific
-------------
:term:`CMOS` specific classes.

CMOSCharacteristics
===================
.. autoclass:: CMOSCharacteristics
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

CMOSGeometry
============
.. autoclass:: CMOSGeometry
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

MKID specific
-------------
:term:`MKID` specific classes.

MKIDCharacteristics
===================
.. autoclass:: MKIDCharacteristics
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

MKIDGeometry
============
.. autoclass:: MKIDGeometry
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

APD specific
------------
:term:`APD` specific classes.

APDCharacteristics
==================
.. autoclass:: APDCharacteristics
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

APDGeometry
===========
.. autoclass:: APDGeometry
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:
