.. _APD architecture:

###
APD
###

API reference: :py:class:`~pyxel.detectors.APD`

Available models
================

* Photon generation
    * :ref:`Load image`
    * :ref:`Simple illumination`
    * :ref:`Stripe pattern`
    * :ref:`Shot noise`
* Optics
    * :ref:`Physical Optics Propagation in PYthon (POPPY)`
    * :ref:`Load PSF`
* Charge generation
    * :ref:`Simple photoconversion`
    * :ref:`Conversion with custom QE map`
    * :ref:`Load charge`
    * :ref:`CosmiX cosmic ray model`
    * :ref:`Dark current`
    * :ref:`Simple dark current`
    * :ref:`APD gain`
    * :ref:`Dark current Saphira`
* Charge collection
    * :ref:`Simple collection`
    * :ref:`Simple full well`
    * :ref:`Fixed pattern noise`
* Charge measurement:
    * :ref:`DC offset`
    * :ref:`Output pixel reset voltage APD`
    * :ref:`kTC reset noise`
    * :ref:`Simple charge measurement`
    * :ref:`Readout noise Saphira`
    * :ref:`Non-linearity (polynomial)`
* Readout electronics:
    * :ref:`Simple ADC`