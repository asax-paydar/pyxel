.. _acknowledgements:

Acknowledgements
================


.. image:: _static/ESA_logo_2020_Deep.png
    :alt: esa
    :scale: 15 %
    :align: center

.. image:: _static/eso_colour.jpg
    :alt: esa
    :scale: 15 %
    :align: center