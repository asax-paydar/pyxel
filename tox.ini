# Tox (https://testrun.org/tox) is a tool for running tests
# Install:
#   pip install tox
# Run:
#   tox
#   tox -av                 # Show list of all defined environments
#   tox -l                  # Show list of test environments
#   tox -e mypy
#   tox -e build
#   tox -p                  # Run all tests in parallel
#   tox -e py-basic         # Run unit tests
#   tox -e py310-basic      # Run unit tests
#   tox -e py-cov-basic     # Run unit tests
#   tox -e py310-cov-basic  # Run unit tests

[tox]
minversion=3.4
envlist = py310-basic,flake8,mypy,doc8,pydocstyle,codespell,docs
skip_missing_interpreters = true

[testenv:py{37,38,39,310,}-{basic,extra_model,extra_calibration,extra_all}]
description = Tests without coverage
deps =
    pytest
    pytest-httpserver
    freezegun
    jsonschema
extras =
    basic: model
    basic: io
    extra_model: model
    extra_calibration: calibration
    extra_all: all
commands =
    pytest --color=yes

[testenv:py{37,38,39,310,}-cov-{basic,extra_model,extra_calibration,extra_all}]
description = Tests without coverage
deps =
    pytest
    pytest-httpserver
    freezegun
    jsonschema
    pytest-cov
extras =
    basic: model
    basic: io
    extra_model: model
    extra_calibration: calibration
    extra_all: all
commands =
    coverage run -m pytest
    coverage report
    coverage xml

[testenv:flake8]
skip_install = true
deps =
    flake8
    pep8-naming
    flake8-bugbear
    tryceratops
commands =
    flake8 pyxel

[testenv:pydocstyle]
skip_install = true
deps =
    pydocstyle>=4

commands =
    pydocstyle pyxel

[testenv:mypy]
deps =
    mypy>=0.910
    types-requests
    types-PyYAML
commands =
    mypy pyxel

[testenv:codespell]
skip_install = true
deps =
    codespell
commands =
    codespell pyxel docs/source

[testenv:doc8]
skip_install = true
deps =
  sphinx
  doc8
commands =
  doc8 --ignore-path docs/source/license.rst docs/source

# Documentation
[testenv:docs]
setenv =
    PYTHONPATH = {toxinidir}
extras =
    model
deps =
    -rdocs/requirements-docs.txt
commands =
    sphinx-build -E -W -b html docs/source docs/html

[testenv:serve-docs]
basepython = python3
skip_install = true
changedir = docs/html
deps =
commands =
    python -m http.server {posargs}

[testenv:bandit]
skip_install = true
deps =
    bandit
commands =
    bandit -r pyxel -f screen --ini .bandit

[testenv:licenses]
skip_install = true
deps =
    scancode-toolkit
    click == 7.1.2
commands =
    scancode --verbose --license --copyright \
             --ignore "*.fits" --ignore "*.npy" --ignore "*.ascii" --ignore "*.data" \
             --ignore "*.txt" --ignore "*.cfg" --ignore "*.pyc" --ignore ".DS_Store" \
             --html license.html pyxel

# Build wheel file
[testenv:build]
skip_install = true
deps =
    wheel
    setuptools
commands =
    ; Create wheel (.whl) and source archive (.tar.gz) files.
    python setup.py sdist bdist_wheel

# Release to 'pypi' with 'tox -e release'
# Release to 'test.pypi' with 'tox -e release -- --repository testpypi'
[testenv:release]
description = Release to Pypi
skip_install = true
passenv =
    TWINE_USERNAME
    TWINE_PASSWORD
    TWINE_REPOSITORY_URL
deps =
    {[testenv:build]deps}
    twine >= 1.11.0
allowlist_externals =
    rm
commands =
    rm -rf dist

    {[testenv:build]commands}

    twine check dist/*
    twine upload --verbose {posargs} --skip-existing dist/*
